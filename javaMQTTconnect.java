package javaMQTTPack;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Artturi
 */
public class javaMQTTconnect {

    // public void sendMsg(String topic, String content) {
//public static void main(String[] args) {
public javaMQTTconnect(String msg){
        String topic        = "mirthTest";
        String content      = msg;
        int qos             = 2;
        String broker       = "tcp://localhost:1883";
        String clientId     = "JavaSample";
        MemoryPersistence persistence = new MemoryPersistence();
        write("Settings ok");

        try {
            MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            write("Connecting to broker: ");
            sampleClient.connect(connOpts);
            write("Connected");
            write("Publishing message: ");
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(qos);
            sampleClient.publish(topic, message);
            write("Message published");
            sampleClient.disconnect();
            write("Disconnected");
            System.exit(0);
        } catch(Exception me) {
            write("reason ");
            write("msg "+me.getMessage());
            write("loc "+me.getLocalizedMessage());
            write("cause "+me.getCause());
            write("excep "+me);
        }
    }
    public void write(String str) {
        try {
            Files.write(Paths.get("/home/artturi/javamqttLog.txt" + System.lineSeparator()), str.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }
}

//}
