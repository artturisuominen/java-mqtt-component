## Readme


As of now the files
  
javaMQTTconnect.java  	
javaMQTTsubscriber.java  
are not used in mirth. 

Only the customMqttCallback.jar and the org.eclipse.paho.client.mqttv3.jar are used. 
It would still be good to get the other java components working or at least know why they failed, 
as using javaMQTTconnect.java to send the message made mirth connect crash.

## Options to pay attention in integrations

###QoS (Quality of service)
either when publishing or when creating mqtt message
> publish(byte[] payload, int qos, boolean retained)

or

> MqttMessage(byte[] payload)  
> setQos(int qos)  
> publish(java.lang.String topic, MqttMessage message)

Quality of Service 0 - indicates that a message should be delivered at most once (zero or one times). The message will not be persisted to disk, and will not be acknowledged across the network. This QoS is the fastest, but should only be used for messages which are not valuable - note that if the server cannot process the message (for example, there is an authorization problem), then an MqttCallback.deliveryComplete(IMqttDeliveryToken). Also known as "fire and forget".


Quality of Service 1 - indicates that a message should be delivered at least once (one or more times). The message can only be delivered safely if it can be persisted, so the application must supply a means of persistence using MqttConnectOptions. If a persistence mechanism is not specified, the message will not be delivered in the event of a client failure. The message will be acknowledged across the network. This is the default QoS.


Quality of Service 2 - indicates that a message should be delivered once. The message will be persisted to disk, and will be subject to a two-phase acknowledgement across the network. The message can only be delivered safely if it can be persisted, so the application must supply a means of persistence using MqttConnectOptions. If a persistence mechanism is not specified, the message will not be delivered in the event of a client failure.


If persistence is not configured, QoS 1 and 2 messages will still be delivered in the event of a network or server problem as the client will hold state in memory. If the MQTT client is shutdown or fails and persistence is not configured then delivery of QoS 1 and 2 messages can not be maintained as client-side state will be lost.

__QoS only provides reliability between client and broker__

###Retain message

If message retaining is used
> MqttMessage.setRetained(boolean retained);


> MqttClient.publish(java.lang.String topic, byte[] payload, int qos, boolean retained);


The last message will be stored by the broker and sent to any connecting/subscribing client.