/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaMQTTPack;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 * @author Artturi
 */
public class javaMQTTsubscriber implements MqttCallback {

    String msg;
    boolean received;

    MqttClient clientsub;
    MqttConnectOptions connOpt;

    public String returnMessage() {
        while (!received) {            
        }
        return msg;
    }

    public javaMQTTsubscriber() throws MqttException {
        String broker = "tcp://localhost:1883"; // Adress MQTT Server
        String clientId = "listenerClient"; // ClientID
        String topic1f = "testi"; // Topic
        int QoSserveur = 2;

        try {

            clientsub = new MqttClient(broker, clientId);
            connOpt = new MqttConnectOptions();

            connOpt.setCleanSession(false);
            connOpt.setKeepAliveInterval(30);

            clientsub.setCallback(this);

            // Connection to MQTT Server
            write("subscribing");
            clientsub.connect(connOpt);

            clientsub.subscribe(topic1f, QoSserveur);
            //System.out.println("here!!");

        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable thrwbl) {
        write("Lost connection");
    }

    @Override
    public void messageArrived(String string, MqttMessage message) throws Exception {
        write("message Arrived: : " + new String(message.getPayload()));
        msg = new String(message.getPayload());
        received = true;
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken imdt) {
        write("delivery Complete");
    }
    /*public static void main(String[] args){
        try {
            javaMQTTsubscriber subClient = new javaMQTTsubscriber();

        } catch (MqttException ex) {
        Logger.getLogger(javaMQTTsubscriber.class.getName()).log(Level.SEVERE, null, ex);
    }
    }*/
        public void write(String str) {
        try {
            Files.write(Paths.get("/home/artturi/javamqttSUBSCRIBERLog.txt" + System.lineSeparator()), str.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }
}
